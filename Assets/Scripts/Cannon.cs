﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour {

	public float couldown;
	private float timeCounter;
	private bool canShoot;

	//Create bullets
	public GameObject bullet;
	private Bullet[] bullets;
	public int maxBullets;
	public Transform bulletPosition;
	private int currentBullet;


	void Start(){
		canShoot = true;
		timeCounter = 0;
		currentBullet = 0;

		CreateBullets ();
	}

	void Update(){
		if (!canShoot) {
			timeCounter += Time.deltaTime;
			if (timeCounter >= couldown) {
				canShoot = true;
			}
		}
	}

	public void Shoot(){
		if (canShoot) {
			canShoot = false;
			timeCounter = 0;

			bullets[currentBullet].Shot(transform.position);
			currentBullet++;

			if(currentBullet >= maxBullets) currentBullet = 0;
		}
	}

	void CreateBullets(){
		bullets = new Bullet[maxBullets];
		for(int i = 0; i < maxBullets; i++)
		{
			Vector2 spawnPos = bulletPosition.position;
			spawnPos.x -= i *0.2f;
			GameObject tmpbullet = Instantiate(bullet, spawnPos, Quaternion.identity, null);
			tmpbullet.name = "Bullet_" + i;

			bullets[i] = tmpbullet.GetComponent<Bullet>();
		}

	}
}
