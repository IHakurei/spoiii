﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	public float speed;
	private bool shooting = false;
	private Vector3 iniPos;

	void Awake(){
		iniPos = transform.position;
	}

	public void Shot(Vector3 position){
		transform.position = position;
		shooting = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (shooting) {
			transform.Translate (0, speed * Time.deltaTime, 0);
		}
	}

	public void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Finish") {
			transform.position = iniPos;
			shooting = false;
		}
	}
}
